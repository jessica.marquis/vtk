#include "QQuickVtkItem.h"
#include "QSGVtkOpenGLNode.h"

#include <QtQuick/QSGRendererInterface>
#include <QtQuick/QSGRenderNode>
#include <QtQuick/QQuickWindow>

#include <QtGui/QOpenGLTextureBlitter>

#include <QtCore/QMap>
#include <QtCore/QQueue>
#include <QtCore/QThread>
#include <QtCore/QRunnable>
#include <QtCore/QSharedPointer>

#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkGenericOpenGLRenderWindow.h"
#include "vtkOpenGLFramebufferObject.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkTextureObject.h"
#include "vtkOpenGLState.h"
#include "vtkRenderer.h"

#include "QVTKInteractorAdapter.h"
#include "QVTKInteractor.h"

#include <limits>
#include <queue>

/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+- */

class QQuickVtkItemPrivate
{
public:
    QQuickVtkItemPrivate(QQuickVtkItem* ptr) : q_ptr(ptr)
    {}

    QQueue<std::function<void(vtkRenderWindow*, QQuickVtkItem::vtkUserData)>> asyncDispatch;

    QVTKInteractorAdapter qt2vtkInteractorAdapter;

    bool ignoreClipRequests = false;

private:
    Q_DISABLE_COPY(QQuickVtkItemPrivate)
    Q_DECLARE_PUBLIC(QQuickVtkItem)
    QQuickVtkItem * const q_ptr;
};

/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+- */

QQuickVtkItem::QQuickVtkItem(QQuickItem* parent) : QQuickItem(parent), d_ptr(new QQuickVtkItemPrivate(this))
{
    setAcceptHoverEvents(true);
#if 0
    setAcceptTouchEvents(true);
#endif
    setAcceptedMouseButtons(Qt::AllButtons);

    setFlag(QQuickItem::ItemIsFocusScope);
    setFlag(QQuickItem::ItemHasContents);
}

QQuickVtkItem::~QQuickVtkItem() = default;

void QQuickVtkItem::dispatch_async(std::function<void(vtkRenderWindow*, vtkUserData)> f)
{
    Q_D(QQuickVtkItem);

    d->asyncDispatch.append(std::move(f));

    update();
}

void QQuickVtkItem::qtRect2vtkViewport(QRectF const& qtRect, double vtkViewport[4], QRectF* glRect)
{
    // Calculate our scaled size
    auto sz = size() * window()->devicePixelRatio();

    // Use a temporary if not supplied by caller
    QRectF tmp; if (!glRect) 
        glRect = &tmp;

    // Convert origin to be bottom-left
    *glRect = QRectF{{qtRect.x(), sz.height() - qtRect.bottom() - 1.0}, qtRect.size()};

    // Convert to a vtkViewport
    if (vtkViewport) {
        vtkViewport[0] = glRect->topLeft    ().x() / (sz.width () - 1.0);
        vtkViewport[1] = glRect->topLeft    ().y() / (sz.height() - 1.0);
        vtkViewport[2] = glRect->bottomRight().x() / (sz.width () - 1.0);
        vtkViewport[3] = glRect->bottomRight().y() / (sz.height() - 1.0);
    };
}

QSGNode* QQuickVtkItem::updatePaintNode(QSGNode* node, UpdatePaintNodeData*)
{
    Q_D(QQuickVtkItem);

    auto * n = static_cast<QSGVtkOpenGLNode *>(node);
    
    // Don't create the node if our size is invalid
    if (!n && (width() <= 0 || height() <= 0))
        return nullptr;

    // Create the QSGRenderNode 
    if (!n) {
        switch (auto api = window()->rendererInterface()->graphicsApi()) {
        case QSGRendererInterface::OpenGL : 
            n = new QSGVtkOpenGLNode;
            break;
        case QSGRendererInterface::OpenGLRhi :  // There is a stale pointer bug in Qt 5.15.2, qsgbatchrenderer.cpp:4564 that prevents OpenGLRhi from working.
            Q_FALLTHROUGH();
        default: 
            qWarning().nospace() << "QQuickVTKItem.cpp:" << __LINE__ << ", Unsupported graphicsApi(): " << api;
            return nullptr;
        }
    }

    // Initializes the VTK window
    if (!n->vtkWindow)  
    {
        // Create and initialize the vtkWindow
        n->vtkWindow = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New();
        n->vtkWindow->SetMultiSamples(0);
        n->vtkWindow->SetReadyForRendering(false);
        n->vtkWindow->SetFrameBlitModeToNoBlit();
        vtkNew<QVTKInteractor> iren;
        iren->SetRenderWindow(n->vtkWindow);
        vtkNew<vtkInteractorStyleTrackballCamera> style;
        iren->SetInteractorStyle(style);
        n->vtkWindow->SetReadyForRendering(false);
        iren->Initialize();
        n->vtkWindow->SetMapped(true);
        n->vtkWindow->SetIsCurrent(true);
        n->vtkWindow->SetForceMaximumHardwareLineWidth(1);
        n->vtkWindow->SetOwnContext(false);
        n->vtkWindow->OpenGLInitContext();

        n->vtkUserData = initializeVTK(n->vtkWindow);
    }

    // Render VTK into pixels
    if (n->vtkWindow) 
    {
        // Calculate our scaled size
        auto sz = size() * window()->devicePixelRatio();

        // Forward size changes to VTK
        n->vtkWindow->SetSize(sz.width(), sz.height());
        n->vtkWindow->GetInteractor()->SetSize(n->vtkWindow->GetSize());

        // Dispatch commands to VTK
        while (d->asyncDispatch.size())
            d->asyncDispatch.dequeue()(n->vtkWindow, n->vtkUserData);

        // Allow derived classes to update VTK state
        syncVTK(n->vtkWindow, n->vtkUserData);

        // Render VTK into it's framebuffer
        auto ostate = n->vtkWindow->GetState();
        ostate->Reset();
        ostate->Push();    
        ostate->vtkglDepthFunc(GL_LEQUAL);          // note: By default, Qt sets the depth function to GL_LESS but VTK expects GL_LEQUAL
        n->vtkWindow->SetReadyForRendering(true);
        n->vtkWindow->GetInteractor()->ProcessEvents();
        n->vtkWindow->GetInteractor()->Render();
        n->vtkWindow->SetReadyForRendering(false);
        ostate->Pop();

        // Synchronize the shared state between the qt-gui-thread and the qsg-render-thread
        n->shared.vtkTextureId = n->vtkWindow->GetDisplayFramebuffer()->GetColorAttachmentAsTextureObject(0)->GetHandle();
        n->shared.qtItemSize = size();
        n->shared.ignoreClipRequest = d->ignoreClipRequests;

        // Mark the node dirty so the qsg-render-thread refreshes the window pixels via render() on our QSGRenderNode
        n->markDirty(QSGRenderNode::DirtyForceUpdate);
    }

    return n;
}

bool QQuickVtkItem::ignoreClipRequest() const
{
    Q_D(const QQuickVtkItem);

    return d->ignoreClipRequests;
}

void QQuickVtkItem::setIgnoreClipRequest(bool v)
{
    Q_D(QQuickVtkItem);

    if (d->ignoreClipRequests != v) {
        update();
        Q_EMIT ignoreClipRequestChanged(d->ignoreClipRequests = v);
    }
}

bool QQuickVtkItem::event(QEvent * ev)
{
    Q_D(QQuickVtkItem);

    if (!ev)
        return false;

    switch (ev->type())
    {
    case QEvent::HoverEnter:
    case QEvent::HoverLeave:
    case QEvent::HoverMove:
    {
        auto e = static_cast<QHoverEvent *>(ev);
        dispatch_async([d, 
            e = QHoverEvent(
                e->type(), 
                e->posF(),
                e->oldPosF(), 
                e->modifiers())]
            (vtkRenderWindow* vtkWindow, vtkUserData) mutable {
                d->qt2vtkInteractorAdapter.ProcessEvent(&e, vtkWindow->GetInteractor());
            });
        break;
    }
    case QEvent::KeyPress:
    case QEvent::KeyRelease:
    {
        auto e = static_cast<QKeyEvent *>(ev);
        dispatch_async([d, 
            e = QKeyEvent(
                e->type(), 
                e->key(), 
                e->modifiers(), 
                e->nativeScanCode(),
                e->nativeVirtualKey(), 
                e->nativeModifiers(), 
                e->text(), 
                e->isAutoRepeat(), 
                e->count())]
            (vtkRenderWindow* vtkWindow, vtkUserData) mutable {
                d->qt2vtkInteractorAdapter.ProcessEvent(&e, vtkWindow->GetInteractor());
            });
        break;
    }
    case QEvent::FocusIn:
    case QEvent::FocusOut:
    {
        auto e = static_cast<QFocusEvent *>(ev);
        dispatch_async([d, 
            e = QFocusEvent(
                e->type(), 
                e->reason())]
            (vtkRenderWindow* vtkWindow, vtkUserData) mutable {
                d->qt2vtkInteractorAdapter.ProcessEvent(&e, vtkWindow->GetInteractor());
            });
        break;
    }
    case QEvent::MouseMove:
    case QEvent::MouseButtonPress:
    case QEvent::MouseButtonRelease:
    case QEvent::MouseButtonDblClick:
    {
        auto e = static_cast<QMouseEvent *>(ev);
        dispatch_async([d, 
            e = QMouseEvent(
                e->type(), 
                e->localPos(),
                e->windowPos(),
                e->screenPos(),
                e->button(), 
                e->buttons(), 
                e->modifiers(),
                e->source())]
            (vtkRenderWindow* vtkWindow, vtkUserData) mutable {
                d->qt2vtkInteractorAdapter.ProcessEvent(&e, vtkWindow->GetInteractor());
            });        
        break;
    }
#ifndef QT_NO_WHEELEVENT
    case QEvent::Wheel:
    {
        auto e = static_cast<QWheelEvent *>(ev);
        dispatch_async([d, 
            e = QWheelEvent(
                e->position(),
                e->globalPosition(), 
                e->pixelDelta(), 
                e->angleDelta(),
                e->buttons(), 
                e->modifiers(), 
                e->phase(), 
                e->inverted(), 
                e->source())]
            (vtkRenderWindow* vtkWindow, vtkUserData) mutable {
                d->qt2vtkInteractorAdapter.ProcessEvent(&e, vtkWindow->GetInteractor());
            });
        break;
    }
#endif
#if 0
    case QEvent::TouchBegin:
    case QEvent::TouchUpdate:
    case QEvent::TouchEnd:
    case QEvent::TouchCancel:
    {
        auto e = static_cast<QTouchEvent *>(ev);
        dispatch_async([d, 
            e = QTouchEvent(e->type(),
                e->device(),
                e->modifiers(),
                e->touchPointStates(),
                e->touchPoints())]
            (vtkRenderWindow* vtkWindow, vtkUserData) mutable {
                d->qt2vtkInteractorAdapter.ProcessEvent(&e, vtkWindow->GetInteractor());
            });
        break;
    }
#endif
    default:
        return QQuickItem::event(ev);
    }

    ev->accept();

    return true;
}
