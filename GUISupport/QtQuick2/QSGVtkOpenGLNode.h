#pragma once

#include <QtQuick/QSGRenderNode>

#include <QtCore/QScopedPointer>

#include <limits>

#include "vtkSmartPointer.h"

class QOpenGLTextureBlitter;

class vtkGenericOpenGLRenderWindow;

class vtkObject;

class QSGVtkOpenGLNode : public QSGRenderNode
{
public:
    ~QSGVtkOpenGLNode() override;

    // QSGRenderNode interface
    void releaseResources() override;
    StateFlags changedStates() const override;
    RenderingFlags flags() const override;
    QRectF rect() const override;
    void render(const RenderState * state) override;

    // Shared state between qsg-render-thread and qt-gui-thread set in QQuickVtkItem::updatePaintNode()
    struct {
        GLuint vtkTextureId = std::numeric_limits<GLuint>::max();
        QSizeF qtItemSize;
        bool ignoreClipRequest = false;
    } shared;

private:
    vtkSmartPointer<vtkGenericOpenGLRenderWindow>  vtkWindow;
    QScopedPointer<QOpenGLTextureBlitter> qtBlitter;
    vtkSmartPointer<vtkObject> vtkUserData;
    friend class QQuickVtkItem;
};
