#pragma once

#include <QtQuick/QQuickItem>

#include <QtCore/QScopedPointer>

#include "vtkGUISupportQtQuick2Module.h" // for export macro
#include "vtkSmartPointer.h"

#include <functional>

class vtkRenderWindow;
class vtkObject;

class QQuickVtkItemPrivate;
class VTKGUISUPPORTQTQUICK2_EXPORT QQuickVtkItem : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(bool ignoreClipRequest READ ignoreClipRequest WRITE setIgnoreClipRequest NOTIFY ignoreClipRequestChanged)

public:
    explicit QQuickVtkItem(QQuickItem* parent = nullptr);
    ~QQuickVtkItem() override;

    // vtkUserData definition
    using vtkUserData = vtkSmartPointer<vtkObject>;

    /**
    * This is where the VTK initializiation should be done including creating a pipeline and attaching it to the window
    *
    * \note All VTK objects must be stored in the vtkUserData object returned from this method.
    *       They will be destroyed if the underlaying QSGRenderNode (which must contain all VTK objects) is destroyed.
    * 
    * \note At any moment the QML SceneGraph can decide to delete the underlying QSGRenderNode. 
    *       If this happens this method will be called again to (re)create all VTK objects used by this node
    * 
    * \note Because of this you must be prepared to reset all the VTK state associated with any QML property
    *       you have attached to this node during this execution of this method.
    *
    * \note At the time of this method execution, the GUI thread is blocked. Hence, it is safe to
    *       perform state synchronization between the GUI elements and the VTK classes here.
    * 
    * \param renderWindow, the VTK render window that creates this object's pixels for display
    * 
    * \return The vtkUserData object associated with the VTK render window
    */
    virtual vtkUserData initializeVTK(vtkRenderWindow *renderWindow) { Q_UNUSED(renderWindow) return {}; }

    /**
    * This is the function called on the QtQuick render thread before the scenegraph state
    * is synchronized. 
    *
    * This is where the pipeline updates, camera manipulations, etc. and other pre-render steps can be performed.
    *
    * \note At the time of this method execution, the GUI thread is blocked. Hence, it is safe to
    * perform state synchronization between the GUI elements and the VTK classes here.
    *
    * \param renderWindow, the VTK render window that creates this object's pixels for display
    * \param userData An optional User Data object associated with the VTK render window
    */
    virtual void syncVTK(vtkRenderWindow *renderWindow, vtkUserData userData) { Q_UNUSED(renderWindow) Q_UNUSED(userData) }

    /**
    * This is the function that enqueues an async command that will be executed just before syncVTK()
    * 
    * \note This function should only be called from the qt-gui-thread, eg. from a QML button click-handler
    *
    * \note At the time of the async command execution, the GUI thread is blocked. Hence, it is safe to
    * perform state synchronization between the GUI elements and the VTK classes in the async command function.
    *
    * \param renderWindow, the VTK render window that creates this object's pixels for display
    * \param userData An optional User Data object associated with the VTK render window
    */
    void dispatch_async(std::function<void(vtkRenderWindow* renderWindow, vtkUserData userData)>);

    /**
    * This function converts a QQuickItem rectangle in local coordinates into the corresponding VTK viewport
    */
    void qtRect2vtkViewport(QRectF const&, double[4], QRectF* = nullptr);

    /**
    * When using a QQuickControls.ComboBox in a scene it can set a bad clip rectangle on this item.
    *
    * Set this to true to ignore scene-graph clip requests
    */
    bool ignoreClipRequest() const;
    void setIgnoreClipRequest(bool);
Q_SIGNALS:
    void ignoreClipRequestChanged(bool);

protected:
    bool event(QEvent*) override;
protected:
    QSGNode* updatePaintNode(QSGNode*, UpdatePaintNodeData*) override;

private:
    Q_DISABLE_COPY(QQuickVtkItem)
    Q_DECLARE_PRIVATE(QQuickVtkItem)
    QScopedPointer<QQuickVtkItemPrivate> d_ptr;
};

#define V_UD(Class,ud,vtk) vtkSmartPointer<Class> const vtk(Class::SafeDownCast(ud))