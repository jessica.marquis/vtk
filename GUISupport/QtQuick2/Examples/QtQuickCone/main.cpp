#include <QtQml/QQmlApplicationEngine>

#include <QtQuick/QQuickWindow>

#include <QtGui/QGuiApplication>
#include <QtGui/QSurfaceFormat>

#include <QQuickVTKItem.h>
#include <QVTKRenderWindowAdapter.h>

#include <vtkActor.h>
#include <vtkPolyDataMapper.h>
#include <vtkConeSource.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>

struct MyVtkItem : QQuickVtkItem
{        
    vtkUserData initializeVTK(vtkRenderWindow* renderWindow) override
    {
        // Create a cone pipeline and add it to the view
        vtkNew<vtkConeSource> cone;

        vtkNew<vtkPolyDataMapper> mapper;
        mapper->SetInputConnection(cone->GetOutputPort());

        vtkNew<vtkActor> actor;
        actor->SetMapper(mapper);

        vtkNew<vtkRenderer> renderer;
        renderer->AddActor(actor);
        renderer->ResetCamera();
        renderer->SetBackground (0.0, 1.0, 1.0);
        renderer->SetBackground2(1.0, 0.0, 0.0);
        renderer->SetGradientBackground(true);
        double vp[4]; qtRect2vtkViewport(boundingRect(), vp); 
        renderer->SetViewport(vp);

        renderWindow->AddRenderer(renderer);
        renderWindow->SetMultiSamples(16);

        return nullptr;
    }
};

int main(int argc, char *argv[])
{
    QQuickWindow::setSceneGraphBackend(QSGRendererInterface::OpenGL);
    QSurfaceFormat::setDefaultFormat(QVTKRenderWindowAdapter::defaultFormat());

#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    qmlRegisterType<MyVtkItem>("com.bluequartz.example", 1, 0, "MyVtkItem");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
