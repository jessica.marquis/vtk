#include <QtQml/QQmlApplicationEngine>

#include <QtQuick/QQuickWindow>

#include <QtGui/QGuiApplication>
#include <QtGui/QSurfaceFormat>

#include <QQuickVTKItem.h>
#include <QVTKRenderWindowAdapter.h>

#include <vtkActor.h>
#include <vtkBoxWidget.h>
#include <vtkCamera.h>
#include <vtkCommand.h>
#include <vtkConeSource.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkTransform.h>

namespace {
class vtkMyCallback : public vtkCommand
{
public:
    static vtkMyCallback* New() 
    { 
        return new vtkMyCallback; 
    }
    void Execute(vtkObject* caller, unsigned long, void*) override 
    {
        vtkNew<vtkTransform> t;
        auto widget = reinterpret_cast<vtkBoxWidget*>(caller);
        widget->GetTransform(t);
        widget->GetProp3D()->SetUserTransform(t);
    }
};

struct MyVtkUserData : vtkObject
{
    vtkSmartPointer<vtkBoxWidget> boxWidget;

    static MyVtkUserData* New() { return new MyVtkUserData; }
    vtkTypeMacro(MyVtkUserData, vtkObject);
};
}

struct MyVtkItem : QQuickVtkItem
{
    vtkUserData initializeVTK(vtkRenderWindow* renderWindow) override
    {
        // Create our vtk user data
        vtkNew<MyVtkUserData> vtk;

        // Create a cone pipeline and add it to the view
        vtkNew<vtkConeSource> cone;
        cone->SetHeight(3.0);
        cone->SetRadius(1.0);
        cone->SetResolution(10);

        vtkNew<vtkPolyDataMapper> mapper;
        mapper->SetInputConnection(cone->GetOutputPort());

        vtkNew<vtkNamedColors> colors;
        vtkNew<vtkActor> actor;
        actor->SetMapper(mapper);
        actor->GetProperty()->SetColor(colors->GetColor3d("Bisque").GetData());

        vtkNew<vtkRenderer> renderer;
        renderer->AddActor(actor);
        renderer->ResetCamera();
        renderer->SetBackground(colors->GetColor3d("LightBlue").GetData());
        renderer->SetBackgroundAlpha(1.0);
        double vp[4]; qtRect2vtkViewport(boundingRect(), vp); 
        renderer->SetViewport(vp);

        renderWindow->AddRenderer(renderer);
        renderWindow->SetMultiSamples(16);

        vtk->boxWidget = vtkSmartPointer<vtkBoxWidget>::New();
        vtk->boxWidget->SetInteractor(renderWindow->GetInteractor());
        vtk->boxWidget->SetPlaceFactor(1.25);
        vtk->boxWidget->GetOutlineProperty()->SetColor(colors->GetColor3d("Gold").GetData());
        vtk->boxWidget->SetProp3D(actor);
        vtk->boxWidget->PlaceWidget();
        vtk->boxWidget->On();

        vtkNew<vtkMyCallback> callback;
        vtk->boxWidget->AddObserver(vtkCommand::InteractionEvent, callback);

        return vtk;
    }

};

int main(int argc, char *argv[])
{
    QQuickWindow::setSceneGraphBackend(QSGRendererInterface::OpenGL);
    QSurfaceFormat::setDefaultFormat(QVTKRenderWindowAdapter::defaultFormat());

#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    qmlRegisterType<MyVtkItem>("com.bluequartz.example", 1, 0, "MyVtkItem");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
